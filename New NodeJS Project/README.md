# NodeJS Project - Google TypeScript (gts)

1. Copy `example.gitignore` file from this directory onto the projects directory and rename it to `.gitignore`
2. `yarn init` to initialize a `package.json`
3. `npx gts init` to initialize the **Google Typescript**
4. Replace `scripts` into `package.json` to these ones

```json
"scripts": {
    "start": "yarn run compile && node ./build/src/index.js",
    "test": "jest",
    "check": "gts check ./src/**/*.ts",
    "clean": "gts clean",
    "compile": "yarn run clean && tsc -p .",
    "fix": "gts fix ./src/**/*.ts",
    "prepare": "npm run compile",
    "pretest": "npm run compile",
    "posttest": "npm run check"
  }
```